import React, { Component } from "react";
import { StyleSheet, View, Image, TextInput } from "react-native";
import Icon from "react-native-vector-icons/EvilIcons";

export default class HeaderSection extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <Image
          source={require("../assets/images/bat.jpg")}
          resizeMode="contain"
          style={styles.UserAvatar}
        />
        <View style={styles.SearchHeader}>
          <Icon name="search" style={styles.SearchIcon} />
          <TextInput placeholder="Search" style={styles.SearchInput} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  UserAvatar: {
    width: 30,
    height: 36,
    borderRadius: 10,
    marginLeft: 10,
    alignSelf: "center"
  },
  SearchHeader: {
    height: 40,
    backgroundColor: "#1a1a1c",
    flexDirection: "row",
    alignItems: "center",
    opacity: 1,
    justifyContent: "flex-start",
    borderRadius: 10,
    flex: 1,
    marginRight: 4,
    marginLeft: 6,
    alignSelf: "center"
  },
  SearchIcon: {
    color: "grey",
    fontSize: 20,
    marginLeft: 5,
    marginRight: 1
  },
  SearchInput: {
    width: 239,
    height: 40,
    color: "rgba(255,255,255,1)",
    marginRight: 1,
    marginLeft: 5,
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular"
  }
});
