import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";

export default class NotificationComponent extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.commentRow}>
          <MaterialCommunityIconsIcon name="comment" style={styles.comment} />
          <View style={styles.NotificationData}>
            <Text style={styles.NotificationHeading}>
              {this.props.NotificationHeading ||
                "u/ABC replied to your post in r/pics."}
            </Text>
            <Text style={styles.notificationContent}>
              {this.props.notificationContent || "You should check it out."}
            </Text>
            <Text style={styles.Time}>
              {this.props.Time || "Post reply • 1h"}
            </Text>
          </View>
        </View>
        <View style={styles.commentRowFiller} />
        <MaterialCommunityIconsIcon
          name="dots-vertical"
          style={styles.MoreIcon}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(0,0,0,1)",
    flexDirection: "row"
  },
  comment: {
    color: "rgba(128,128,128,1)",
    fontSize: 20,
    alignSelf: "flex-start",
    height: 20,
    width: 20,
    marginTop: 2
  },
  NotificationData: {
    width: 267,
    height: 77,
    alignSelf: "center",
    marginLeft: 14
  },
  NotificationHeading: {
    color: "rgba(255,255,255,1)",
    fontSize: 16,
    fontFamily: "ibm-plex-sans-regular"
  },
  notificationContent: {
    color: "rgba(128,128,128,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    marginTop: 22
  },
  Time: {
    color: "rgba(128,128,128,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    marginTop: 6
  },
  commentRow: {
    height: 77,
    flexDirection: "row",
    marginLeft: 20,
    marginTop: 14
  },
  commentRowFiller: {
    flex: 1,
    flexDirection: "row"
  },
  MoreIcon: {
    color: "grey",
    fontSize: 20,
    alignSelf: "flex-start",
    height: 20,
    width: 20,
    marginRight: 8,
    marginTop: 16
  }
});
