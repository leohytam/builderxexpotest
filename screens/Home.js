import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from "react-native";
import HeaderSection from "../components/HeaderSection";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import IoniconsIcon from "react-native-vector-icons/Ionicons";
import MaterialIconsIcon from "react-native-vector-icons/MaterialIcons";
import Divider from "../components/Divider";
import PostComponent from "../components/PostComponent";
import Footer from "../components/Footer";

export default class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerSectionColumn}>
          <HeaderSection style={styles.headerSection} />
          <View style={styles.HeaderTabs}>
            <View style={styles.HomeTab}>
              <Text style={styles.HomeText}>Home</Text>
            </View>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Popular")}
              style={styles.button}
            >
              <Text style={styles.PopularText}>Popular</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.LayoutOptions}>
            <View style={styles.BestPostIconRowRow}>
              <View style={styles.BestPostIconRow}>
                <MaterialCommunityIconsIcon
                  name="rocket"
                  style={styles.BestPostIcon}
                />
                <Text style={styles.BestPosts}>BEST POSTS</Text>
                <IoniconsIcon
                  name="md-arrow-dropdown"
                  style={styles.DropdownIcon}
                />
              </View>
              <View style={styles.BestPostIconRowFiller} />
              <MaterialIconsIcon name="view-agenda" style={styles.LayoutIcon} />
            </View>
            <Divider style={styles.divider} />
          </View>
        </View>
        <View style={styles.PostSection}>
          <ScrollView
            horizontal={false}
            contentContainerStyle={styles.PostSection_contentContainerStyle}
          >
            <PostComponent
              Subreddit="r/Popular"
              PostDetails="Posted by u/hey • 10 hours ago"
              ContentText="What is an odd behaviour of yours you think only you do?"
              UpvotesText="873"
              CommentText="91"
              style={styles.postComponent}
            />
            <PostComponent
              ContentText="Mayank Agarwal has scored his maiden double century - which is also his first century - in his 5th test match and the first match in India"
              Subreddit="r/Cricket"
              UpvotesText="873"
              CommentText="91"
              PostDetails="Posted by u/hey • 5 hours ago"
              text5=""
              style={styles.postComponent2}
            />
            <PostComponent
              ContentText="What is an odd behaviour of yours you think only you do?"
              Subreddit="r/AMA"
              UpvotesText="873"
              CommentText="91"
              PostDetails="Posted by u/hey • 10 hours ago"
              style={styles.postComponent3}
            />
            <PostComponent
              ContentText="Betty Marion white was born in 1922; the first BMW car was made in 1928. Betty White is the original BMW"
              Subreddit="r/ShowerThoughts"
              UpvotesText="34.3k"
              CommentText="280"
              PostDetails="Posted by u/dinger • 10 hours ago"
              style={styles.postComponent4}
            />
            <PostComponent
              ContentText="Betty Marion white was born in 1922; the first BMW car was made in 1928. Betty White is the original BMW"
              Subreddit="r/ShowerThoughts"
              UpvotesText="34.3k"
              CommentText="280"
              PostDetails="Posted by u/dinger • 10 hours ago"
              style={styles.postComponent5}
            />
          </ScrollView>
        </View>
        <Footer style={styles.footer} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,1)"
  },
  headerSection: {
    height: 60
  },
  HeaderTabs: {
    height: 48,
    backgroundColor: "rgba(0,0,0,1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 6
  },
  HomeTab: {
    width: 124,
    height: 48,
    borderColor: "#026bbd",
    borderWidth: 0,
    borderBottomWidth: 3,
    justifyContent: "center"
  },
  HomeText: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    alignSelf: "center"
  },
  button: {
    width: 142,
    alignSelf: "stretch",
    justifyContent: "center"
  },
  PopularText: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    alignSelf: "center"
  },
  LayoutOptions: {
    height: 48,
    marginTop: 1
  },
  BestPostIcon: {
    color: "grey",
    fontSize: 20
  },
  BestPosts: {
    color: "rgba(128,128,128,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-700",
    letterSpacing: 1,
    marginLeft: 11,
    marginTop: 3
  },
  DropdownIcon: {
    color: "grey",
    fontSize: 20,
    marginLeft: 21
  },
  BestPostIconRow: {
    height: 20,
    flexDirection: "row"
  },
  BestPostIconRowFiller: {
    flex: 1,
    flexDirection: "row"
  },
  LayoutIcon: {
    color: "grey",
    fontSize: 20
  },
  BestPostIconRowRow: {
    height: 20,
    flexDirection: "row",
    marginTop: 14,
    marginLeft: 15,
    marginRight: 26
  },
  divider: {
    height: 1,
    marginTop: 13
  },
  headerSectionColumn: {
    marginTop: 22
  },
  PostSection: {
    backgroundColor: "rgba(0,0,0,1)",
    flex: 1
  },
  PostSection_contentContainerStyle: {
    flexDirection: "column",
    justifyContent: "flex-start"
  },
  postComponent: {
    height: 120,
    alignSelf: "stretch"
  },
  postComponent2: {
    height: 120,
    alignSelf: "stretch"
  },
  postComponent3: {
    height: 120,
    alignSelf: "stretch"
  },
  postComponent4: {
    height: 120,
    alignSelf: "stretch"
  },
  postComponent5: {
    height: 120,
    alignSelf: "stretch"
  },
  footer: {
    height: 50
  }
});
