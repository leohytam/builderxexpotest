import React, { Component } from "react";
import { StyleSheet, View, Text, ScrollView, Image } from "react-native";
import HeaderSection from "../components/HeaderSection";
import EntypoIcon from "react-native-vector-icons/Entypo";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import ZocialIcon from "react-native-vector-icons/Zocial";
import Footer from "../components/Footer";

export default class Explore extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerSectionColumn}>
          <HeaderSection style={styles.headerSection} />
          <View style={styles.ExploreTabs}>
            <View style={styles.rect2}>
              <Text style={styles.text}>Subscriptions</Text>
            </View>
            <View style={styles.rect3}>
              <Text style={styles.text2}>Custom Feed</Text>
            </View>
          </View>
          <Text style={styles.RecentlyVisited}>
            Recently visited subreddits
          </Text>
          <View style={styles.scrollAreaStack}>
            <View style={styles.scrollArea}>
              <ScrollView
                horizontal={true}
                contentContainerStyle={styles.scrollArea_contentContainerStyle}
              >
                <View style={styles.rect6}>
                  <View style={styles.rect5}>
                    <Image
                      source={require("../assets/images/gita.jpg")}
                      resizeMode="cover"
                      style={styles.image}
                    />
                    <Text style={styles.text7}>27.2k members</Text>
                    <Text style={styles.text4}>r/Art</Text>
                  </View>
                </View>
                <View style={styles.rect7}>
                  <View style={styles.rect5}>
                    <Image
                      source={require("../assets/images/gita.jpg")}
                      resizeMode="cover"
                      style={styles.image}
                    />
                    <Text style={styles.text7}>27.2k members</Text>
                    <Text style={styles.text4}>r/AskReddit</Text>
                  </View>
                </View>
                <View style={styles.rect8}>
                  <View style={styles.rect5}>
                    <Image
                      source={require("../assets/images/gita.jpg")}
                      resizeMode="cover"
                      style={styles.image}
                    />
                    <Text style={styles.text7}>27.2k members</Text>
                    <Text style={styles.text4}>r/Jokes</Text>
                  </View>
                </View>
              </ScrollView>
            </View>
            <View style={styles.CommunitiesWrapper}>
              <View style={styles.All}>
                <View style={styles.AllIconRow}>
                  <View style={styles.AllIcon}>
                    <EntypoIcon name="bar-graph" style={styles.icon} />
                  </View>
                  <Text style={styles.AllText}>All</Text>
                </View>
              </View>
              <Text style={styles.Communities}>COMMUNITIES</Text>
              <View style={styles.OtherCommunities}>
                <View style={styles.rect14}>
                  <View style={styles.rect15Row}>
                    <View style={styles.rect15}>
                      <MaterialCommunityIconsIcon
                        name="reddit"
                        style={styles.icon3}
                      />
                    </View>
                    <Text style={styles.text11}>r/announcements</Text>
                  </View>
                </View>
                <View style={styles.rect16}>
                  <View style={styles.rect17Row}>
                    <View style={styles.rect17}>
                      <MaterialCommunityIconsIcon
                        name="reddit"
                        style={styles.icon4}
                      />
                    </View>
                    <Text style={styles.text12}>r/AMA</Text>
                  </View>
                </View>
                <View style={styles.rect18}>
                  <View style={styles.rect19Row}>
                    <View style={styles.rect19}>
                      <MaterialCommunityIconsIcon
                        name="reddit"
                        style={styles.icon5}
                      />
                    </View>
                    <Text style={styles.text13}>r/Batman</Text>
                  </View>
                </View>
                <View style={styles.rect20}>
                  <View style={styles.rect21Row}>
                    <View style={styles.rect21}>
                      <MaterialCommunityIconsIcon
                        name="reddit"
                        style={styles.icon6}
                      />
                    </View>
                    <Text style={styles.text14}>r/Bandersnatch</Text>
                  </View>
                </View>
                <View style={styles.rect22}>
                  <View style={styles.rect23Row}>
                    <View style={styles.rect23}>
                      <ZocialIcon name="android" style={styles.icon7} />
                    </View>
                    <Text style={styles.text15}>r/Android</Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.headerSectionColumnFiller} />
        <Footer style={styles.footer} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,1)"
  },
  headerSection: {
    height: 60
  },
  ExploreTabs: {
    height: 48,
    backgroundColor: "rgba(0,0,0,1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 6
  },
  rect2: {
    width: 158,
    alignSelf: "stretch",
    borderColor: "#026bbd",
    borderWidth: 0,
    borderBottomWidth: 3,
    justifyContent: "center"
  },
  text: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    alignSelf: "center"
  },
  rect3: {
    width: 163,
    alignSelf: "stretch"
  },
  text2: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    marginTop: 14,
    alignSelf: "center"
  },
  RecentlyVisited: {
    color: "#808080",
    fontSize: 16,
    marginTop: 18,
    marginLeft: 22
  },
  scrollArea: {
    top: 0,
    width: 360,
    height: 143,
    backgroundColor: "rgba(0,0,0,1)",
    position: "absolute",
    flexDirection: "row",
    alignItems: "center",
    left: 0
  },
  scrollArea_contentContainerStyle: {},
  rect6: {
    width: 156,
    height: 128,
    marginLeft: 16
  },
  rect5: {
    backgroundColor: "rgba(100,85,85,1)",
    opacity: 0.14,
    borderRadius: 10,
    flex: 1
  },
  image: {
    width: 156,
    height: 64,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  text7: {
    color: "rgba(255,255,255,1)",
    fontSize: 10,
    marginTop: 27,
    alignSelf: "center"
  },
  text4: {
    color: "rgba(255,255,255,1)",
    fontSize: 12,
    fontFamily: "ibm-plex-sans-500",
    letterSpacing: 1,
    marginTop: -35,
    marginLeft: 58
  },
  rect7: {
    width: 156,
    height: 128,
    marginLeft: 16
  },
  rect8: {
    width: 156,
    height: 128,
    marginLeft: 16
  },
  CommunitiesWrapper: {
    top: 139,
    left: 22,
    width: 290,
    height: 347,
    position: "absolute",
    justifyContent: "space-around"
  },
  All: {
    width: 62,
    height: 27,
    flexDirection: "row"
  },
  AllIcon: {
    width: 26,
    height: 27,
    backgroundColor: "#ff4500",
    borderRadius: 100,
    justifyContent: "center"
  },
  icon: {
    color: "rgba(255,255,255,1)",
    fontSize: 20,
    alignSelf: "center"
  },
  AllText: {
    color: "rgba(255,255,255,1)",
    fontSize: 20,
    fontFamily: "ibm-plex-sans-regular",
    marginLeft: 12,
    marginTop: 3
  },
  AllIconRow: {
    height: 27,
    flexDirection: "row",
    flex: 1
  },
  Communities: {
    color: "#7f8182",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-700"
  },
  OtherCommunities: {
    width: 218,
    height: 238,
    justifyContent: "space-around"
  },
  rect14: {
    height: 30,
    alignSelf: "stretch",
    flexDirection: "row"
  },
  rect15: {
    width: 30,
    height: 30,
    backgroundColor: "#ff4500",
    borderRadius: 100,
    justifyContent: "center"
  },
  icon3: {
    color: "rgba(255,255,255,1)",
    fontSize: 20,
    alignSelf: "center"
  },
  text11: {
    color: "rgba(255,255,255,1)",
    fontSize: 16,
    fontFamily: "ibm-plex-sans-regular",
    marginLeft: 16,
    marginTop: 7
  },
  rect15Row: {
    height: 30,
    flexDirection: "row",
    flex: 1,
    marginRight: 45
  },
  rect16: {
    height: 30,
    alignSelf: "stretch",
    flexDirection: "row"
  },
  rect17: {
    width: 30,
    height: 30,
    backgroundColor: "#ff4500",
    borderRadius: 100,
    justifyContent: "center"
  },
  icon4: {
    color: "rgba(255,255,255,1)",
    fontSize: 20,
    alignSelf: "center"
  },
  text12: {
    color: "rgba(255,255,255,1)",
    fontSize: 16,
    fontFamily: "ibm-plex-sans-regular",
    marginLeft: 16,
    marginTop: 7
  },
  rect17Row: {
    height: 30,
    flexDirection: "row",
    flex: 1,
    marginRight: 127
  },
  rect18: {
    height: 30,
    alignSelf: "stretch",
    flexDirection: "row"
  },
  rect19: {
    width: 30,
    height: 30,
    backgroundColor: "rgba(236,27,12,1)",
    borderRadius: 100,
    justifyContent: "center"
  },
  icon5: {
    color: "rgba(255,255,255,1)",
    fontSize: 20,
    alignSelf: "center"
  },
  text13: {
    color: "rgba(255,255,255,1)",
    fontSize: 16,
    fontFamily: "ibm-plex-sans-regular",
    marginLeft: 16,
    marginTop: 7
  },
  rect19Row: {
    height: 30,
    flexDirection: "row",
    flex: 1,
    marginRight: 104
  },
  rect20: {
    height: 30,
    alignSelf: "stretch",
    flexDirection: "row"
  },
  rect21: {
    width: 30,
    height: 30,
    backgroundColor: "rgba(74,144,226,1)",
    borderRadius: 100,
    justifyContent: "center"
  },
  icon6: {
    color: "rgba(255,255,255,1)",
    fontSize: 20,
    alignSelf: "center"
  },
  text14: {
    color: "rgba(255,255,255,1)",
    fontSize: 16,
    fontFamily: "ibm-plex-sans-regular",
    marginLeft: 16,
    marginTop: 7
  },
  rect21Row: {
    height: 30,
    flexDirection: "row",
    flex: 1,
    marginRight: 60
  },
  rect22: {
    height: 30,
    alignSelf: "stretch",
    flexDirection: "row"
  },
  rect23: {
    width: 30,
    height: 30,
    backgroundColor: "rgba(169,203,8,1)",
    borderRadius: 100,
    justifyContent: "center"
  },
  icon7: {
    color: "rgba(255,255,255,1)",
    fontSize: 20,
    alignSelf: "center"
  },
  text15: {
    color: "rgba(255,255,255,1)",
    fontSize: 16,
    fontFamily: "ibm-plex-sans-regular",
    marginLeft: 16,
    marginTop: 7
  },
  rect23Row: {
    height: 30,
    flexDirection: "row",
    flex: 1,
    marginRight: 104
  },
  scrollAreaStack: {
    width: 360,
    height: 486,
    marginTop: 18
  },
  headerSectionColumn: {
    marginTop: 22
  },
  headerSectionColumnFiller: {
    flex: 1
  },
  footer: {
    height: 50
  }
});
