import React, { Component } from "react";
import { StyleSheet, View, Image, Text, ScrollView } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import IoniconsIcon from "react-native-vector-icons/Ionicons";
import Divider from "../components/Divider";
import NotificationComponent from "../components/NotificationComponent";
import Footer from "../components/Footer";

export default class Notifications extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.HeaderColumn}>
          <View style={styles.Header}>
            <View style={styles.imageRow}>
              <Image
                source={require("../assets/images/bat.jpg")}
                resizeMode="contain"
                style={styles.image}
              />
              <Text style={styles.text4}>Inbox</Text>
            </View>
            <View style={styles.imageRowFiller} />
            <MaterialCommunityIconsIcon
              name="dots-vertical"
              style={styles.icon}
            />
          </View>
          <View style={styles.TabSection}>
            <View style={styles.NotifsTab}>
              <Text style={styles.text}>Notifications</Text>
            </View>
            <View style={styles.MessagesTab}>
              <Text style={styles.text2}>Messages</Text>
            </View>
            <View style={styles.ModMailTab}>
              <Text style={styles.text3}>Mod mail</Text>
            </View>
          </View>
          <View style={styles.AllActivity}>
            <View style={styles.icon2Row}>
              <MaterialCommunityIconsIcon name="flash" style={styles.icon2} />
              <Text style={styles.text5}>ALL ACTIVITY</Text>
              <IoniconsIcon name="md-arrow-dropdown" style={styles.icon3} />
            </View>
          </View>
        </View>
        <View style={styles.dividerStack}>
          <Divider style={styles.divider} />
          <View style={styles.scrollArea}>
            <ScrollView
              horizontal={false}
              contentContainerStyle={styles.scrollArea_contentContainerStyle}
            >
              <View style={styles.TrendingNotif}>
                <MaterialCommunityIconsIcon
                  name="trending-up"
                  style={styles.icon4}
                />
                <View style={styles.Content}>
                  <Text style={styles.text6}>Trending on r/FlutterDev</Text>
                  <Text style={styles.text7}>Flutter web image picker</Text>
                  <Text style={styles.text8}>Trending • 1h</Text>
                </View>
              </View>
              <Divider style={styles.divider2} />
              <NotificationComponent style={styles.notificationComponent} />
              <Divider style={styles.divider3} />
              <NotificationComponent
                Time="Comment reply • 2mo"
                NotificationHeading="u/bcd replied to your comment in r/bangalore"
                notificationContent="Right. "
                style={styles.notificationComponent2}
              />
              <Divider style={styles.divider4} />
              <NotificationComponent
                NotificationHeading="u/moderator replied to you comment in r/AMA"
                notificationContent="Please read this message"
                style={styles.notificationComponent3}
              />
              <Divider style={styles.divider5} />
              <NotificationComponent
                NotificationHeading="u/moderator replied to you comment in r/AMA"
                notificationContent="Please read this message"
                style={styles.notificationComponent4}
              />
              <Divider style={styles.divider6} />
              <NotificationComponent
                NotificationHeading="u/moderator replied to you comment in r/AMA"
                notificationContent="Please read this message"
                style={styles.notificationComponent5}
              />
              <Divider style={styles.divider7} />
            </ScrollView>
          </View>
          <Footer style={styles.footer} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,1)"
  },
  Header: {
    height: 60,
    flexDirection: "row"
  },
  image: {
    width: 35,
    height: 36,
    borderRadius: 10
  },
  text4: {
    color: "rgba(255,255,255,1)",
    fontSize: 20,
    fontFamily: "ibm-plex-sans-regular",
    marginLeft: 19,
    marginTop: 8
  },
  imageRow: {
    height: 36,
    flexDirection: "row",
    marginLeft: 18,
    marginTop: 12
  },
  imageRowFiller: {
    flex: 1,
    flexDirection: "row"
  },
  icon: {
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    marginRight: 21,
    marginTop: 18
  },
  TabSection: {
    height: 58,
    flexDirection: "row",
    justifyContent: "space-around",
    borderColor: "rgba(128,128,128,1)",
    borderWidth: 0,
    borderBottomWidth: 1,
    marginTop: 9
  },
  NotifsTab: {
    width: 127,
    height: 59,
    alignSelf: "center",
    marginRight: 0,
    marginLeft: 0,
    borderColor: "#036bbd",
    borderWidth: 0,
    borderBottomWidth: 3,
    justifyContent: "center"
  },
  text: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    alignSelf: "center"
  },
  MessagesTab: {
    width: 101,
    height: 59,
    alignSelf: "center",
    marginRight: 0,
    marginLeft: 0,
    justifyContent: "center"
  },
  text2: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    alignSelf: "center"
  },
  ModMailTab: {
    width: 95,
    height: 59,
    alignSelf: "center",
    marginRight: 0,
    marginLeft: 0,
    justifyContent: "center"
  },
  text3: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    marginLeft: 10
  },
  AllActivity: {
    width: 187,
    height: 26,
    flexDirection: "row",
    marginTop: 14,
    marginLeft: 15
  },
  icon2: {
    color: "rgba(128,128,128,1)",
    fontSize: 20
  },
  text5: {
    color: "rgba(128,128,128,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-700",
    marginLeft: 10,
    marginTop: 3
  },
  icon3: {
    color: "grey",
    fontSize: 20,
    marginLeft: 36
  },
  icon2Row: {
    height: 20,
    flexDirection: "row",
    flex: 1,
    marginRight: 18,
    marginTop: 3
  },
  HeaderColumn: {
    marginTop: 22
  },
  divider: {
    top: 15,
    left: 0,
    height: 1,
    position: "absolute",
    right: 0
  },
  scrollArea: {
    top: 0,
    left: 0,
    backgroundColor: "rgba(0,0,0,1)",
    position: "absolute",
    right: 0,
    bottom: 49,
    justifyContent: "flex-start"
  },
  scrollArea_contentContainerStyle: {},
  TrendingNotif: {
    height: 110,
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "stretch",
    justifyContent: "space-around"
  },
  icon4: {
    color: "rgba(2,107,189,1)",
    fontSize: 20,
    width: 20,
    marginTop: 20,
    alignSelf: "flex-start"
  },
  Content: {
    width: 294,
    height: 80,
    alignSelf: "center",
    marginTop: 10
  },
  text6: {
    color: "rgba(255,255,255,1)",
    fontSize: 16,
    fontFamily: "ibm-plex-sans-regular"
  },
  text7: {
    color: "rgba(128,128,128,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    marginTop: 13
  },
  text8: {
    color: "rgba(128,128,128,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    marginTop: 13
  },
  divider2: {
    height: 1,
    alignSelf: "stretch"
  },
  notificationComponent: {
    height: 110,
    alignSelf: "stretch"
  },
  divider3: {
    height: 1,
    alignSelf: "stretch"
  },
  notificationComponent2: {
    height: 110,
    alignSelf: "stretch"
  },
  divider4: {
    height: 1,
    alignSelf: "stretch"
  },
  notificationComponent3: {
    height: 110,
    alignSelf: "stretch"
  },
  divider5: {
    height: 1,
    alignSelf: "stretch"
  },
  notificationComponent4: {
    height: 110,
    alignSelf: "stretch"
  },
  divider6: {
    height: 1,
    alignSelf: "stretch"
  },
  notificationComponent5: {
    height: 120,
    alignSelf: "stretch"
  },
  divider7: {
    height: 1,
    alignSelf: "stretch"
  },
  footer: {
    left: 0,
    height: 50,
    position: "absolute",
    right: 0,
    bottom: 0
  },
  dividerStack: {
    flex: 1
  }
});
