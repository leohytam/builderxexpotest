import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  Image,
  ImageBackground
} from "react-native";
import HeaderSection from "../components/HeaderSection";
import SimpleLineIconsIcon from "react-native-vector-icons/SimpleLineIcons";
import IoniconsIcon from "react-native-vector-icons/Ionicons";
import MaterialIconsIcon from "react-native-vector-icons/MaterialIcons";
import Divider from "../components/Divider";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import PostComponent from "../components/PostComponent";
import Footer from "../components/Footer";

export default class Popular extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerSectionColumn}>
          <HeaderSection style={styles.headerSection} />
          <View style={styles.HeaderTabs}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Home")}
              style={styles.button}
            >
              <Text style={styles.HomeText}>Home</Text>
            </TouchableOpacity>
            <View style={styles.PopularTab}>
              <Text style={styles.PopularText}>Popular</Text>
            </View>
          </View>
          <View style={styles.LayoutOptionsStack}>
            <View style={styles.LayoutOptions}>
              <View style={styles.BestPostIconRow}>
                <SimpleLineIconsIcon name="fire" style={styles.BestPostIcon} />
                <Text style={styles.BestPosts}>HOT POSTS</Text>
                <IoniconsIcon
                  name="md-arrow-dropdown"
                  style={styles.DropdownIcon}
                />
              </View>
              <View style={styles.BestPostIconRowFiller} />
              <MaterialIconsIcon name="view-agenda" style={styles.LayoutIcon} />
            </View>
            <Divider style={styles.divider} />
          </View>
          <View style={styles.TrendingIconRow}>
            <MaterialCommunityIconsIcon
              name="trending-up"
              style={styles.TrendingIcon}
            />
            <Text style={styles.TrendingToday}>Trending today</Text>
          </View>
          <View style={styles.TrendingSection}>
            <ScrollView
              horizontal={true}
              contentContainerStyle={
                styles.TrendingSection_contentContainerStyle
              }
            >
              <View style={styles.Cover1}>
                <ImageBackground
                  source={require("../assets/images/gita.jpg")}
                  resizeMode="cover"
                  style={styles.image}
                >
                  <ImageBackground
                    style={styles.rect}
                    source={require("../assets/images/Gradient_wLDqew1.png")}
                  >
                    <Text style={styles.text2}>Gita</Text>
                  </ImageBackground>
                </ImageBackground>
              </View>
              <View style={styles.Cover2}>
                <ImageBackground
                  source={require("../assets/images/lights.jpg")}
                  resizeMode="cover"
                  style={styles.image}
                >
                  <ImageBackground
                    style={styles.rect}
                    source={require("../assets/images/Gradient_wLDqew1.png")}
                  >
                    <Text style={styles.text2}>Northern Lights</Text>
                  </ImageBackground>
                </ImageBackground>
              </View>
              <View style={styles.rect2}>
                <ImageBackground
                  source={require("../assets/images/lights.jpg")}
                  resizeMode="cover"
                  style={styles.image}
                >
                  <ImageBackground
                    style={styles.rect}
                    source={require("../assets/images/Gradient_wLDqew1.png")}
                  >
                    <Text style={styles.text2}>Northern Lights</Text>
                  </ImageBackground>
                </ImageBackground>
              </View>
            </ScrollView>
          </View>
        </View>
        <View style={styles.scrollArea}>
          <ScrollView
            horizontal={false}
            contentContainerStyle={styles.scrollArea_contentContainerStyle}
          >
            <PostComponent style={styles.postComponent} />
            <PostComponent
              ContentText="Mayank Agarwal has scored his maiden double century - which is also his first century - in his 5th test match and the first match in India"
              Subreddit="r/Cricket"
              UpvotesText="873"
              CommentText="91"
              PostDetails="Posted by u/hey • 5 hours ago"
              style={styles.postComponent2}
            />
            <PostComponent
              ContentText="What is an odd behaviour of yours you think only you do?"
              Subreddit="r/AMA"
              UpvotesText="873"
              CommentText="91"
              PostDetails="Posted by u/hey • 10 hours ago"
              style={styles.postComponent3}
            />
            <PostComponent
              ContentText="Betty Marion white was born in 1922; the first BMW car was made in 1928. Betty White is the original BMW"
              Subreddit="r/ShowerThoughts"
              UpvotesText="34.3k"
              CommentText="280"
              PostDetails="Posted by u/dinger • 10 hours ago"
              style={styles.postComponent4}
            />
          </ScrollView>
        </View>
        <Footer style={styles.footer} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,1)"
  },
  headerSection: {
    height: 60
  },
  HeaderTabs: {
    height: 48,
    backgroundColor: "rgba(0,0,0,1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 6
  },
  button: {
    width: 124,
    alignSelf: "stretch",
    justifyContent: "center"
  },
  HomeText: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    alignSelf: "center"
  },
  PopularTab: {
    width: 142,
    alignSelf: "stretch",
    borderColor: "#036bbd",
    borderWidth: 0,
    borderBottomWidth: 3,
    justifyContent: "center"
  },
  PopularText: {
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-regular",
    alignSelf: "center"
  },
  LayoutOptions: {
    top: 0,
    left: 0,
    height: 47,
    position: "absolute",
    right: 0,
    flexDirection: "row"
  },
  BestPostIcon: {
    color: "grey",
    fontSize: 20
  },
  BestPosts: {
    color: "rgba(128,128,128,1)",
    fontSize: 14,
    fontFamily: "ibm-plex-sans-700",
    letterSpacing: 1,
    marginLeft: 5,
    marginTop: 3
  },
  DropdownIcon: {
    color: "grey",
    fontSize: 20,
    marginLeft: 28
  },
  BestPostIconRow: {
    height: 20,
    flexDirection: "row",
    marginLeft: 15,
    marginTop: 14
  },
  BestPostIconRowFiller: {
    flex: 1,
    flexDirection: "row"
  },
  LayoutIcon: {
    color: "grey",
    fontSize: 20,
    marginRight: 20,
    marginTop: 14
  },
  divider: {
    top: 47,
    left: 0,
    height: 1,
    position: "absolute",
    right: 0
  },
  LayoutOptionsStack: {
    height: 48,
    marginTop: 1
  },
  TrendingIcon: {
    color: "#036bbd",
    fontSize: 20
  },
  TrendingToday: {
    color: "#036bbd",
    fontSize: 16,
    fontFamily: "ibm-plex-sans-500",
    letterSpacing: 1,
    marginLeft: 20
  },
  TrendingIconRow: {
    height: 20,
    flexDirection: "row",
    marginTop: 13,
    marginLeft: 13,
    marginRight: 184
  },
  TrendingSection: {
    width: 360,
    height: 143,
    backgroundColor: "rgba(0,0,0,1)",
    marginTop: 12
  },
  TrendingSection_contentContainerStyle: {
    width: 1800,
    height: 143,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  Cover1: {
    width: 156,
    height: 128,
    marginLeft: 16,
    borderRadius: 10
  },
  image: {
    width: 156,
    height: 128,
    borderRadius: 10
  },
  rect: {
    opacity: 0.14,
    borderRadius: 10,
    flex: 1
  },
  text2: {
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    fontFamily: "ibm-plex-sans-500",
    letterSpacing: 1,
    marginBottom: 5,
    alignSelf: "center"
  },
  Cover2: {
    width: 156,
    height: 128,
    marginLeft: 16,
    borderRadius: 10
  },
  rect2: {
    width: 156,
    height: 128,
    marginLeft: 16,
    borderRadius: 10
  },
  headerSectionColumn: {
    marginTop: 22
  },
  scrollArea: {
    backgroundColor: "rgba(0,0,0,1)",
    flex: 1,
    marginBottom: 11
  },
  scrollArea_contentContainerStyle: {
    flexDirection: "column"
  },
  postComponent: {
    height: 120,
    alignSelf: "stretch"
  },
  postComponent2: {
    height: 120,
    alignSelf: "stretch"
  },
  postComponent3: {
    height: 120,
    alignSelf: "stretch"
  },
  postComponent4: {
    height: 120,
    alignSelf: "stretch"
  },
  footer: {
    height: 50
  }
});
